import { createContext, useContext, useReducer, useEffect } from 'react';
import products from '../data/products';
import cartReducer from '../reducer/cartReducer';

//สร้าง context คือ การสร้างคลังข้อมูล
const CartContext = createContext()
//กำหนด state เริ่มต้นที่จะใช้งานใน context
const initState={
    products:products,
    total:0,
    amount:0
}

//สร้าง Provider มี parameter ชื่อ children
export const CartProvider=({children})=>{
    //useReducer มี 2 ค่า ค่าเเรกคือ reducer ที่เราจะใช้งาน ค่าที่สองคือ state ที่จะนำไปใช้ใน reducer
    //state คือข้อมูลที่เราจะจัดการเอามาจาก initState
    //dispatch คือการทำงานกับข้อมูลดังกล่าวโดยจะเรียกใช้งาน action ต่างๆที่อยู่ใน cartReducer
    const [state, dispatch] = useReducer(cartReducer, initState)
    function formatMoney(money){
        return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }
    function removeItem(id){
        //payload คือ เงื่อนไขการทำงาน
        dispatch({type: "REMOVE", payload: id})
    }
    function addQuantity(id){
        dispatch({type: "ADD", payload: id})
    }
    function subTractQuantity(id){
        dispatch({type: "SUBTRACT", payload: id})
    }
    useEffect(() =>{
        console.log("เเสดงผลรวม")
        //type คือ ชื่อรูปแบบจัดการ state
        dispatch({type:"CALCULATE_TOTAL"})
    }, [state.products])
    return(
        <CartContext.Provider value={{...state, formatMoney, removeItem, addQuantity, subTractQuantity}}>
            {children}
        </CartContext.Provider>
    )
}

//นำเอา context ไปใช้งานด้านนอก
export const useCart=()=>{
    return useContext(CartContext)
}