const cartReducer = (state, action) => {
  //กระบวนการจัดการ state ผ่าน action
  //action คือ การกระทำ
  if (action.type === "CALCULATE_TOTAL") {
    //cartTotal สามารถเข้าถึงค่าเริ่มต้นเเต่ละตัวได้
    //item เป้นสมาชิกเเต่ละตัวที่อยู่ใน state.product
    const { total, amount } = state.products.reduce((cartTotal, item) => {
        const { price, quantity } = item;
        const totalPrice = price * quantity; //ยอดรวมสินค้าเเต่ละรายการ
        cartTotal.total += totalPrice; //จำนวนเงินรวม
        cartTotal.amount += quantity; //ปริมาณสินค้า
        return cartTotal
      },
      {
        //ค่าเริ่มต้น
        total: 0,
        amount: 0,
      }
    );
    return{
        ...state, 
        total, 
        amount
    }
  }
  if(action.type === "REMOVE"){
    return{
        ...state,
        products:state.products.filter((item)=> item.id !== action.payload)
    }
  }
  if(action.type === "ADD"){
    let updateProduct = state.products.map((item)=>{
        if(item.id === action.payload){
            return{
                ...item,
                quantity: item.quantity+1
            }
        }
        return item;
    })
    return{
        ...state,
        products:updateProduct
    }
  }
  if(action.type === "SUBTRACT"){
    let updateProduct = state.products.map((item)=>{
        if(item.id === action.payload){
            return{
                ...item,
                quantity: item.quantity-1
            }
        }
        return item;
    }) //.filter((item)=>item.quantity !==0 )
    return{
        ...state,
        products:updateProduct
    }
  }
};
export default cartReducer;
