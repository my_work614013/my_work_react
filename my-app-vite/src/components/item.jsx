import "./item.css"
import { useState } from "react";

export default function Item(props) {
  // eslint-disable-next-line react/prop-types
  const { title, description } = props;
  const [show, setShow] = useState(false)
  return (
    <section>
      <div className="content">
        <h4>{title}</h4>
        <button onClick={()=> setShow(!show)}>{show ? "ซ่อน" : "เเสดง"}</button>
      </div>
      {show && <p>{description}</p>}
    </section>
  );
}
