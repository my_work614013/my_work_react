const MenuData = [
    {
        menu : "ผัด-ทอด",
        foodName : "ผัดไทย",
        image_url:"https://cdn.pixabay.com/photo/2021/01/22/18/00/shrimp-5940735_960_720.jpg"
    },
    {
        menu : "ผัด-ทอด",
        foodName : "ไก่ทอด",
        image_url:"https://cdn.pixabay.com/photo/2017/09/03/01/17/wings-2709068_960_720.jpg"
    },
    {
        menu : "ผัด-ทอด",
        foodName : "กะเพราไข่ดาว",
        image_url:"https://cdn.pixabay.com/photo/2015/08/26/10/58/the-pork-fried-rice-made-908333_960_720.jpg"
    },
    {
        menu : "สเต็ก",
        foodName : "สเต็กปลาแซลม่อน",
        image_url:"https://cdn.pixabay.com/photo/2016/06/28/17/32/salmon-1485014_960_720.jpg"
    },
    {
        menu : "สเต็ก",
        foodName : "สเต็กไก่",
        image_url:"https://cdn.pixabay.com/photo/2017/07/16/12/39/chicken-2509164_960_720.jpg"
    },
    {
        menu : "แกง-ต้มยำ",
        foodName : "แกงเขียวหวาน",
        image_url:"https://cdn.pixabay.com/photo/2017/06/30/04/58/green-curry-2457236_960_720.jpg"
    },
    {
        menu : "แกง-ต้มยำ",
        foodName : "ต้มยำกุ้ง",
        image_url:"https://cdn.pixabay.com/photo/2017/04/23/07/36/tom-yum-goong-2253171_960_720.jpg"
    },
    {
        menu : "เครื่องดื่ม",
        foodName : "น้ำส้ม",
        image_url:"https://cdn.pixabay.com/photo/2017/01/20/14/59/orange-1995044__340.jpg"
    },
    {
        menu : "เครื่องดื่ม",
        foodName : "ชามะนาว",
        image_url:"https://cdn.pixabay.com/photo/2016/10/09/17/06/ice-lemon-tea-1726270_960_720.jpg"
    },
    {
        menu : "สเต็ก",
        foodName : "สเต็กเนื้อ",
        image_url:"https://cdn.pixabay.com/photo/2018/09/14/11/12/food-3676796_960_720.jpg"
    },
    {
        menu : "สเต็ก",
        foodName :"โทมาฮอว์ค",
        image_url:"https://cdn.pixabay.com/photo/2019/07/16/20/07/steak-4342500_960_720.jpg"
    },
    {
        menu : "เครื่องดื่ม",
        foodName : "ลาเต้",
        image_url:"https://media.istockphoto.com/id/516984673/th/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%96%E0%B9%88%E0%B8%B2%E0%B8%A2/%E0%B8%96%E0%B9%89%E0%B8%A7%E0%B8%A2%E0%B8%81%E0%B8%B2%E0%B9%81%E0%B8%9F%E0%B8%9E%E0%B8%A7%E0%B8%87%E0%B8%A1%E0%B8%B2%E0%B8%A5%E0%B8%B1%E0%B8%A2%E0%B8%9A%E0%B8%99%E0%B8%9E%E0%B8%B7%E0%B9%89%E0%B8%99%E0%B8%AB%E0%B8%A5%E0%B8%B1%E0%B8%87%E0%B8%AA%E0%B8%B5%E0%B8%82%E0%B8%B2%E0%B8%A7.jpg?s=612x612&w=0&k=20&c=vwsRGLwXK3jDDGUQyDC5OMDttSVMrwPzE52oacBG4Ss="
    }
]

export default MenuData