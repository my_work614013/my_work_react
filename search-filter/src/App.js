import "./App.css";
import { useState, useEffect } from "react";

function App() {
  const [country, setCountry] = useState([]);
  const [word, setWord] = useState("");
  const [dataFilter] = useState(["name", "capital"]);

  // useEffect(() => {
  //   fetch("https://restcountries.com/v3.1/all")
  //     .then((res) => res.json())
  //     .then((data) => {
  //       const sortedCountries = data.sort((a, b) =>
  //         a.name.common.localeCompare(b.name.common)
  //       );
  //       console.log(sortedCountries)
  //       setCountry(sortedCountries);
  //     });
  // }, []);

  useEffect(()=>{
    fetch("https://restcountries.com/v2/all")//update link api
    .then(res=>res.json())
    .then(data=>{
      setCountry(data)
    })
  },[])

  //ฟังก์ชั่นแปลงรูปแบบตัวเลข
  const formatNumber = (num) => {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  };

  function searchCountry(country) {
    return country.filter((item) => {
      // eslint-disable-next-line
      return dataFilter.some((filter) => {
        if (item[filter]) {
          return (
            item[filter].toString().toLowerCase().indexOf(word.toLowerCase()) > -1
          );
        }
      });
    });
  }

  return (
    <div className="container">
      <div className="search-container">
        <label htmlFor="search-form">
          <input
            type="text"
            className="search-input"
            placeholder="ค้นหาข้อมูลประเทศ (เมืองหลวง, ประเทศ)"
            value={word}
            onChange={(e) => setWord(e.target.value)}
          />
        </label>
      </div>
      <ul className="row">
        {searchCountry(country).map((item, index) => {
          return (
            <li key={index}>
              <div className="card">
                <div className="card-title">
                  <img src={item.flag} alt={item.name} />
                </div>
                <div className="card-body">
                  <div className="card-description">
                    <h2>{item.name}</h2>
                    <ol className="card-list">
                      <li>
                        ประชากร : <span>{formatNumber(item.population)} คน</span> 
                      </li>
                      <li>
                        ภูมิภาค : <span>{item.region}</span>
                      </li>
                      <li>
                        เมืองหลวง : <span>{item.capital}</span>
                      </li>
                    </ol>
                  </div>
                </div>
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
}

export default App;