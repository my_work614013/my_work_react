const ImageData = [
    {
        title:"ทะเล",
        image:"https://cdn.pixabay.com/photo/2016/10/21/14/50/plouzane-1758197_1280.jpg"
    },
    {
        title:"ทะเลสาบ",
        image:"https://cdn.pixabay.com/photo/2016/08/11/23/48/mountains-1587287_1280.jpg"
    },
    {
        title:"ภูเขา",
        image:"https://cdn.pixabay.com/photo/2018/08/12/15/29/hintersee-3601004_1280.jpg"
    },
    {
        title:"ลูกแมว",
        image:"https://cdn.pixabay.com/photo/2014/11/30/14/11/cat-551554_1280.jpg"
    }
]

export default ImageData