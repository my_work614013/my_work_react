import { useState } from "react";
import ImageData from "./imageData";
import { AiOutlineArrowLeft } from "react-icons/ai";
import { AiOutlineArrowRight } from "react-icons/ai";

export default function ImageSlider() {
  const [current, setCurrent] = useState(0);
  const length = ImageData.length;

  function prevSlide() {
    current === 0 ? setCurrent(length - 1) : setCurrent(current - 1);
  }

  function nextSlide() {
    current === length - 1 ? setCurrent(0) : setCurrent(current + 1);
  }

  return (
    <section className="slider">
      <AiOutlineArrowLeft className="leftArrow" onClick={prevSlide} />
      <AiOutlineArrowRight className="rightArrow" onClick={nextSlide} />
      {ImageData.map((data, index) => {
        return (
          <div
            className={index === current ? "slide active" : "slide"}
            key={index}
          >
            {index === current && (
              <div>
                <img src={data.image} alt={data.title} className="image" />
                <p>{data.title}</p>
              </div>
            )}
          </div>
        );
      })}
    </section>
  );
}
