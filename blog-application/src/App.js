import Home from "./components/Home";
import Blogs from "./components/Blogs";
import About from "./components/About";
import Navbar from "./components/Navbar";
import NotFound from "./components/NotFound";
import Details from "./components/Details";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom"

function App() {
  return (
    //ตัวกำหนดเส้นทาง หรือบอกว่ากำลังอยู่ที่ url อะไร เป็นตัวสลับ path หรือ url  ---->  BrowserRouter
    //จัดกลุ่ม url ทำกำลังงานอยู่ใน application ผ่าน Routes
    //Routes จะระบุกลุ่มของ url ผ่าน Route
    //Route จะบอกว่าตอนนี้ url ใน application มร path อะไรบ้างเเล้ว path ดังกล่าวไปผูกกับ components อะไร
    //path="*" คือถ้าผู้ใช้กรอก path นอกเหนือจาก / /blogs /about จะเเสดง <NotFound/> ออกมา
    <BrowserRouter> 
    <Navbar/>
      <Routes>
        <Route path="/" element={<Home/>}></Route>
        <Route path="/blogs" element={<Blogs/>}></Route>
        <Route path="/about" element={<About/>}></Route>
        <Route path="*" element={<NotFound/>}></Route>
        <Route path="/home" element={<Navigate to="/"/>}></Route>
        <Route path="/info" element={<Navigate to="/about"/>}></Route>
        <Route path="/blog/:id" element={<Details/>}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
