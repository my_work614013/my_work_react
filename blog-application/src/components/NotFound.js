import notfound from "../images/notfound.svg";

export default function NotFound() {
  return (
    <div className="container">
      <h3 className="title">ไม่พบเว็ปเพจ (404 Page Not Found)</h3>
      <img src={notfound} alt="notfound" />
    </div>
  );
}
