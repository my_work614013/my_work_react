export default function Picture(props){
    return(
        <>
             {/*eslint-disable-next-line react/prop-types*/}
            <img src={props.urls.small} alt={props.description}/>
        </>
    )
}